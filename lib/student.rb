class Student

  def initialize(first_name, last_name, courses=[])
    @first_name = first_name
    @last_name = last_name
    @courses = courses
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def courses
    @courses
  end

  def enroll(course_object)
    if @courses.any? { |course| course.conflicts_with?(course_object) }
      raise "SCHEDULE CONFLICT"
    else
      @courses << course_object unless self.courses.include?(course_object)
    end
    @courses[-1].students << self
  end

  def course_load
    department_with_credits = Hash.new(0)
    @courses.each do |course|
      department_with_credits[course.department] += course.credits
    end
    department_with_credits
  end

end
